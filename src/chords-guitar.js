'use strict';

class Chord {
// X32010: C major
  #tab = null;
  // array of data, e.g. data = [
  //   [], [3], [2], [0], [1], [0],
  // ]
  // where empty array represents the 'X' on tab
  // This allows to display two notes on a single string, e.g. [ 0, 12 ]
  _data = [];
  constructor(data) {
    if (typeof(data) == 'string') {
      // e.g. data = 'X32010'
      this.tab = data;
      for (let string_number in data) {
        if (data[string_number] == 'X') {
          // 'X' is a muted string, store as empty array
          this._data.push([]);
        } else {
          this._data.push([ parseInt(data[string_number]) ]);
        }
      }
    } else {
      // e.g. data = [ [], [3], [2], [0], [1], [0] ];
      this._data = data;
      //throw 'A chord can only be initialized with a string currently';
    }
  };
  GetTab() { return this.#tab; };
  GetData() { return this._data; };
}; // class Chord


class Fingerboard {
  // Methods:
  constructor(canvas_id, settings = {
  }) {
    this.canvas = document.getElementById(canvas_id);
    this.c = this.canvas.getContext('2d');
    this.colors = null;

    // Settings
    this.allFretNumbers = true;
    this.margin = 20;
    this.circleRadius = 5;
    this.isVertical = true;
    this.isMirror = true;
    this.isLong = false;
    this.fretStart = 1;
    this.fretEnd = 22;

    // Fretboard and data
    this.countStrings = 6;
    this.chord = null;

    // Calculated
    this.canvas.height = 120; // TODO: hardcoded
    this.canvas.width = 120; // TODO: hardcoded
    this.countFrets = 22;
    this.fbLength = 0; // fretboard length
    this.fbWidth = 0; // fretboard width
    this.W = 0;
    this.H = 0;
    this.CalculateDimensions();
  }

  //////////////// PUBLIC ////////////////////
  CalculateDimensions() {
    this.countFrets = this.fretEnd - this.fretStart + 1;
    const margin = this.margin;
    // vertical vs horizontal
    let W = this.canvas.height;
    let H = this.canvas.width;
    if (this.isVertical) {
      [W, H] = [H, W]
    }
    this.W = W;
    this.H = H;
    this.fbHeight = H - 2*margin;
    this.fbWidth = W - 2*margin;
    this.fretWidth = this.fbWidth/(this.countStrings-1);
    this.fretHeight = this.fbHeight/this.countFrets;
  }


  DrawFingerboard() {
    // Draw strings and frets
    const c = this.c;
    for (let string = 1; string <= this.countStrings; string++) {
      // Strings
      c.lineWidth = (this.countStrings-string+1)/2;
      this.DrawLineCoord(
        this.Coord(string, 0),
        this.Coord(string, this.countFrets),
      );
    }
    // TODO: Clean
    //for (let fret = this.fretStart; fret <= this.fretEnd; fret++) {
    for (let fret = 0; fret <= this.countFrets; fret++) {
      // Frets
      c.lineWidth = (fret == 0)? 3 : 1;
      this.DrawLineCoord(
        this.Coord(1, fret),
        this.Coord(6, fret),
      );
    }
  };


  DrawLine(x1, y1, x2, y2) {
    let c = this.c;
    c.beginPath();
    c.moveTo(x1, y1);
    c.lineTo(x2, y2);
    c.stroke();
  };


  DrawLineCoord(a, b) {
    // a, b = Coord
    let c = this.c;
    c.beginPath();
    c.moveTo(a.x, a.y);
    c.lineTo(b.x, b.y);
    c.stroke();
  };


  FlipOrientation() {
    this.isVertical = !this.isVertical;
    [this.canvas.height, this.canvas.width] = [this.canvas.width, this.canvas.height];
    this.DrawChord(this.chord);
  }


  FlipLong() {
    this.isLong = !this.isLong;
    if (this.isLong) {
      this.canvas.height = 120;
      this.canvas.width = window.innerWidth-2*this.margin;
      this.fretStart = 1;
      this.fretEnd = 22;
      if (this.isVertical) {
        this.canvas.height = window.innerWidth-2*this.margin;
        this.canvas.width = 120;
      }
    } else {
      this.fretStart = 1;
      this.fretEnd = 5;
      this.canvas.height = 120;
      this.canvas.width = 120;
    }
    this.DrawChord(this.chord);
  }


  FlipMirror() {
    this.isMirror = !this.isMirror;
    this.DrawChord(this.chord);
  }


  Clear() {
    const canvas = this.canvas;
    const c = this.c;
    c.clearRect(0, 0, canvas.width, canvas.height);
  }


  SetColor(color) {
    const c = this.c;
    c.fillStyle = color;
    c.strokeStyle = color;
  }


  SetColors(colors) {
    this.colors = colors;
  }


  ClearColors() {
    this.colors = null;
  }


  SetChord(chord) {
    this.chord = chord;
  }


  SetFrets(fretStart, fretEnd) {
    this.fretStart = fretStart;
    this.fretEnd = fretEnd;
    this.DrawChord(this.chord);
  }


  DrawChord(chord, colors=[]) {
    // chord is of class Chord
    // colors = []
    // colors = [ ['blue'], ['green'], ['red'], ['red'], ['red'], ['red'] ]
    this.CalculateDimensions();
    this.chord = chord;
    this.Clear();
    this.DrawFingerboard();
    let ch_data = chord.GetData();
    for (let i = 0; i < ch_data.length; i++) {
      let string_number = i+1;
      if (ch_data[i].length == 0) {
        // Zeroth fret/open string: crosses
        let coords = this.CoordFinger(string_number, 0);
        this.DrawCross(coords.x, coords.y);
      } else {
        // Draw fingers/circles
        for (let j = 0; j < ch_data[i].length; j++) {
          let fret = ch_data[i][j];
          let coords = this.CoordFinger(string_number, fret-this.fretStart+1); // DEBUG
          let is_full = (fret == 0)? false : true;
          if (this.colors != null) {
            this.SetColor(this.colors[i][j]);
          }
          if (this.colors != null) {
            this.SetColor(this.colors[i]);
          }
          this.DrawCircle(coords.x, coords.y, is_full);
          this.SetColor('black'); // reset
        }
      }
    }
    this.DrawFretNumbers();
  };


  DrawFretNumbers() {
    // Draw text
    // TODO: text.width here is hardcoded
    const c = this.c;
    const allFrets = this.allFretNumbers;
    const countFrets = this.countFrets;
    let dots = [ 1, 3, 5, 7, 9, 12, 15, 17, 19, 21 ];
    for (let i = 1; i<= countFrets; i++) {
      let j = i + this.fretStart - 1;
      if (!allFrets && !dots.includes(j)) {
        continue;
      }
      if (i == 0) continue;
      let text = c.measureText(j);
      let coo = this.Coord(0, i, true);
      let x = coo.x;
      let y = coo.y;
      if (this.isVertical) {
        // vertical
        x = x - text.width/2;
        y = y + text.width/2;
      } else {
        // horizontal
        x = x - text.width/2;
        y = this.margin/2;
      }
      c.fillText(j, x, y);
    }
  };


  DrawCircleCoord(coord, is_full=true) {
    // coord = Coord
    this.DrawCircle(coord.x, coord.y, is_full);
  };


  DrawCircleColor(x, y, is_full, color) {
    const c = this.c;
    let bakFill = c.fillStyle;
    let bakStroke = c.strokeStyle;
    this.SetColor(color);
    this.DrawCircle(x, y, is_full);
    this.SetColor(bakFill);
  }

  DrawCircle(x, y, is_full=true) {
    // Draw a circle at given coords (full or not)
    const c = this.c;
    c.beginPath();
    c.arc(x, y, this.circleRadius, 0, 2 * Math.PI);
    if (is_full) {
      c.fill();
    } else {
      c.lineWidth = 3;
      c.stroke();
    }
    c.lineWidth = 1; // reset
  }


  DrawCross(x, y) {
    // Draw a cross at a given centre (x,y)
    const c = this.c;
    let r = this.circleRadius;
    let offset = (r/2)*Math.sqrt(2)+1; // hardcoded 1
    c.lineWidth = 3;
    this.DrawLine( // line 1: top-left -> bottom-right
      x - offset, y - offset,
      x + offset, y + offset,
    );
    this.DrawLine( // line 2: bottom-left -> top-right
      x - offset, y + offset,
      x + offset, y - offset,
    );
    c.lineWidth = 1;
  };


  CoordFinger(string, fret) {
    // These values are offset because the finger presses between two frets,
    // and not directly on the fret
    // return { x, y }
    return this.Coord(string, fret, true);
  }
  Coord(string, fret, is_offset=false) {
    const fretWidth = this.fretWidth;
    const fretHeight = this.fretHeight;
    const countFrets = this.countFrets;
    const countStrings = this.countStrings;
    const margin = this.margin;
    const isVertical = this.isVertical;
    const isMirror = this.isMirror;
    let x = 0,
        y = 0;
    // HORIZ
    // *_offset: This is for isMirror and isVertical settings:
    //           frets start from left to right or from right to left
    let string_offset = (isMirror)? string - 1 : countStrings - string;
    let fret_offset = (isVertical)? fret : countFrets - fret;
    // horizontal
    x = margin + fretHeight*fret_offset;
    y = margin + fretWidth*string_offset;
    if (is_offset) {
      x = (isVertical)? x-= fretHeight/2 : x+= fretHeight/2;
      if (fret == 0) {
        // Zero fret is on the margin
        x = (isVertical)? margin/2 : this.H - margin/2;
      }
    }
    if (this.isVertical) {
      [x, y] = [y, x]
    }
    return { x: x, y: y };
  }
  MouseToFretboard(e) {
    // input: MouseEvent
    // return: { string, fret }
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = (e.clientX - rect.left)*(canvas.width/canvas.clientWidth);
    const mouseY = (e.clientY - rect.top)*(canvas.height/canvas.clientHeight);
    let fret = Math.floor(this.countFrets - (mouseX-this.margin)/this.fretHeight);
    let string = Math.floor((mouseY+this.margin)/this.fretWidth)-1;
    if (!this.isMirror) {
      // TODO: Can this be a neater function?
      switch (string) {
        case 1: string = 6; break;
        case 2: string = 5; break;
        case 3: string = 4; break;
        case 4: string = 3; break;
        case 5: string = 2; break;
        case 6: string = 1; break;
      }
    }
    if (string > this.countStrings
      || string < 1
      || fret > this.countFrets-1
      || fret < -1
    ) {
      return null;
    }
    return {
      string: string,
      //fret: fret-this.fretStart+2,
      fret: fret+this.fretStart,
    }
  }


  addCanvasEventListener(type, func) {
    // User-provided callback for the canvas
    this.canvas.addEventListener(type, func.bind(this));
  }

}; //class TabDraw
