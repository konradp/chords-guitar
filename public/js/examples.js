function _create(tag) {
  return document.createElement(tag);
}

function Run() {
  let examples = document.getElementsByTagName('example');
  for (let ex of examples) {
    let name = ex.getAttribute('src');
    let url = 'js/'+ name + '.js';
    fetch(url)
      .then((response => response.text()))
      .then(data => {
        // Create the table with code and output
        // output cell id = 'js/example1.js'
        let tbl = _create('table'),
            thead = _create('thead'),
            tr = _create('tr'),
            td_code = _create('td'),
            td_output = _create('td'),
            pre = _create('pre'),
            code = _create('code');
        thead.innerHTML = '<th>code</th><th>output</th>';
        tbl.className = 'tblcode';
        tbl.appendChild(thead);
        code.innerHTML = data;
        code.className = 'language-js';
        hljs.highlightElement(code); // highlightjs
        pre.appendChild(code);
        td_code.appendChild(pre);
        td_code.className = 'td-code';
        td_output.id = name;
        tr.appendChild(td_code);
        tr.appendChild(td_output);
        tbl.appendChild(tr);
        ex.appendChild(tbl);

        // Run the code: unsafe eval
        eval(data);
      })
  }
  console.log('Highlighting');
}

