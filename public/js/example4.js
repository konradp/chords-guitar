let div = document.getElementById('example4');
let canvas = document.createElement('canvas');
canvas.id = 'ex4-canvas';
div.appendChild(canvas);

// G red, A blue, B green
let t = new Fingerboard(canvas.id);
t.SetColors([
  ['red', 'blue'],
  ['blue', 'green'],
  ['red'],
  ['red', 'blue', 'green'],
  ['blue'],
  ['red', 'blue'],
]);
t.DrawChord(new Chord([
  [3,5],
  [0,2],
  [5],
  [0,2,4],
  [],
  [3,5],
]));
