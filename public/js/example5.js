const id = 'example5';
let div = document.getElementById(id);
let canvas = document.createElement('canvas');
canvas.id = id + '-canvas';
div.appendChild(canvas);

// G red, A blue, B green
let t = new Fingerboard(canvas.id);
t.DrawChord(new Chord([
  [3,6],
  [3,5],
  [3,5],
  [3,5],
  [3,6],
  [3,6],
]));
t.FlipOrientation();
t.FlipMirror();
