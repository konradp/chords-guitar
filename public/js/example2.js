let div = document.getElementById('example2');
let canvas = document.createElement('canvas');
canvas.id = 'ex2-canvas';
div.appendChild(canvas);

let t = new Fingerboard(canvas.id);
t.DrawChord(new Chord([ [],[3],[2],[0],[1],[0] ]));
t.FlipOrientation();
