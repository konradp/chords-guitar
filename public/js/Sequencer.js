'use strict';

class Sequencer {
// Methods:
// - StartPause()
// - Stop()
// - SetBpm(85)
// - SetSequence('HL--H-L')
constructor() {
  // Private
  this.time_now = 0
  this.pDelay = 0
  this.pTimeout = null
  this.state = 0
  this.pSeqLength = 16; // TODO: This is wrong, might go out of sync with Canvas
  this.hitsPerBeat = 4
  this.sequence = []
  this.SetBpm(85)
  this.player = new Player();
  this.player.load_sounds([
    [ 'sounds/01.ogg' ],
    [ 'sounds/02.wav' ],
    [ 'sounds/03.wav' ],
    [ 'sounds/04.ogg' ]
  ]);
}

//////////////// PUBLIC ////////////////////
/* TRANSPORT: Start/Stop/Pause */
StartPause() {
  if(!this.pTimeout) {
    // Start
    this.pTimeout = setInterval(
      this.pTick,
      this.pDelay
    )
  } else {
    // Pause
    clearInterval(this.pTimeout)
    this.pTimeout = null
  }
}

Stop() {
  clearInterval(this.pTimeout)
  this.pTimeout = null;
  self.state = 0;
}

/* Getters */
GetSequence() {
  return this.sequence;
}

/* Setters */
SetBpm(value) {
  // TODO: Setting BPM pauses the sequencer but does not resume afterwards
  if(this.pTimeout) clearInterval(this.pTimeout)
  if(value < 1) value = 1
  if(value > 500) value = 500

  // Note duration: ( 60 sec ) / BPM / sequenceLength
  this.StartPause();
  this.pDelay = (60.0 * 1000.0) / value / this.hitsPerBeat
  this.StartPause();
}

SetSequenceStr(str) {
  // str = '0--1---0';
  var seq = [];
  const n = str.length
  for(let i = 0; i < n; i ++) {
    let instrument = str[i];
    if (instrument != '-') {
      seq.push({ instrument: instrument, time: i });
    }
  }
  this.sequence = seq;
}

SetSequence(seq) {
  // seq = [
  //   { instrument: 0, time: 1 },
  //   { instrument: 0, time: 2 },
  //   ...
  // ]
  this.sequence = seq;
}

//////////////// PRIVATE ////////////////////
// Note: From within callbacks, 'this' may not be what you expect
// i.e. it will not necessarily be 'this class', but can be e.g. 'this window'
// We need to get an instance of our class
pTick() {
  self = GetSeqInstance()
  let pos = self.state
  if(pos >= self.pSeqLength) pos = 0;
  self.state = pos + 1
  const shouldPlay = (arr, time) => { return arr.filter(i => i.time == time); };
  shouldPlay(self.sequence, pos).forEach((event) => {
    if (this.debug) console.log('Note is', event.instrument);
    self.player.Play(event.instrument, 0); // TODO: Schedule this instead
  });
}

}; //class Sequencer
