// Create canvas element
let div = document.getElementById('example1');
let canvas = document.createElement('canvas');
canvas.id = 'ex1-canvas';
div.appendChild(canvas);

// Create chord
let t = new Fingerboard(canvas.id);
t.DrawChord(new Chord('X32010'));
